/*
	upc码：（639382 00039 3) 前6位企业代码、后面5位商品编码、最后以为校验码
	校验码生成算法：10 - ((奇数位的和 * 3 + 偶数位的和)% 10)
*/
package main

import (
	"bufio"
	"fmt"
	"os"
	"io/ioutil"
	"encoding/json"
)
// 定义配置文件解析后的结构
type UpcConfig struct {
	CompanyCode		int
	Start			int
	End				int
}

type JsonStruct struct {

}

func NewJsonStruct() *JsonStruct {
	return &JsonStruct{}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
func (jst *JsonStruct) Load(filename string, v interface{}){
	//ioutil.ReadFile函数会读取文件的全部内容，并将结果以[]byte类型返回
	data, err := ioutil.ReadFile(filename)
	check(err)

	err = json.Unmarshal(data, v)
	check(err)
}

func gen(code int, num int) int{
	base := code * 100000 + num

	i := 0
	oddSum := 0
	evenSum := 0
	for temp := base; temp != 0; temp /= 10 {
		i++
		if i % 2 == 0 {
			evenSum += temp % 10
		} else{
			oddSum += temp % 10
		}
	}
	sum := oddSum * 3 + evenSum

	return base * 10 + (10 - sum % 10)
}

func main(){
	JsonParse := NewJsonStruct()
	config := UpcConfig{}
	JsonParse.Load("./config.json", &config)

	//写入文件
	file, err := os.Create("upc.txt")
	check(err)
	defer file.Close()

	writer := bufio.NewWriter(file)
	defer writer.Flush()

	for i := config.Start; i <= config.End; i++ {
		fmt.Fprint(writer, gen(config.CompanyCode, i), "\r\n")
	}
}